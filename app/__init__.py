from flask import Flask
from flask_restx import Api
from flask_migrate import Migrate

from app.config import DevelopmentConfig
from app.database import db
from app.dao.model.director import Director
from app.dao.model.genre import Genre
from app.dao.model.movie import Movie
from app.views.movie import movie_ns
from app.views.director import director_ns
from app.views.genre import genre_ns


def create_app():
    app = Flask(__name__)
    app.config.from_object(DevelopmentConfig)
    register_extensions(app)
    return app


def register_extensions(app):
    db.init_app(app)
    migrate = Migrate(app, db)
    api = Api(app)
    api.add_namespace(movie_ns)
    api.add_namespace(director_ns)
    api.add_namespace(genre_ns)



