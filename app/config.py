from pathlib import Path


BASE_DIR = Path(__file__)



class Config:
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///dbase.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True
    DEVELOPMENT = True


class ProdactionConfig(Config):
    ...
    
