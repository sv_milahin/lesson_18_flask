from flask_marshmallow import Marshmallow
from app.database import db


ma = Marshmallow()


class Director(db.Model):
    __tablename__ = 'directors'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))


class DirectorSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name')


director_schema = DirectorSchema()
directors_schema = DirectorSchema(many=True)
