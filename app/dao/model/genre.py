from flask_marshmallow import Marshmallow
from app.database import db


ma = Marshmallow()


class Genre(db.Model):
    __tablename__ = 'genres'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))


class GenreSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name')


genre_schema = GenreSchema()
genres_schema = GenreSchema(many=True)

