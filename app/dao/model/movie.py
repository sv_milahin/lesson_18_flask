from app.database import db
from flask_marshmallow import Marshmallow
from app.dao.model.genre import Genre
from app.dao.model.director import Director


ma = Marshmallow()


class Movie(db.Model):
    __tablename__ = 'movies'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(521))
    description = db.Column(db.Text(1024))
    trailer = db.Column(db.String(512))
    year = db.Column(db.Integer)
    rating = db.Column(db.String(32))
    genre_id = db.Column(db.Integer, db.ForeignKey(Genre.id))
    genre = db.relationship(Genre)
    director_id = db.Column(db.Integer, db.ForeignKey(Director.id))
    directot = db.relationship(Director)


class MovieSchema(ma.Schema):
    class Meta:
        fields = ("id", "title", "description", "trailer", "year", "rating", "genre_id", "director_id")
        


movie_schema = MovieSchema()
movies_schema = MovieSchema(many=True)

