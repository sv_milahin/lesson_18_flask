from flask import request
from flask_restx import Resource, Namespace

from app.container import director_service
from app.dao.model.director import director_schema, directors_schema

director_ns = Namespace('directors')


@director_ns.route('/')
class DirectorView(Resource):

    def get(self):
        director = director_service.get_all()
        return directors_schema.dump(director), 200

    def post(self):
        req = request.get_json()
        director = director_service.create(req)
        return '', 201, {'location': f'/directors/{director}'}


@director_ns.route('/<int:pk>')
class DirectorVies(Resource):

    def get(self, pk):
        director = director_service.get_one(pk)
        return director_schema.dump(director), 200

    def put(self, pk):
        req = request.get_json()
        req['id'] = pk 
       
        director_service.update(req)
        return '', 204

    def delete(self, pk):
        director_service.delete(pk)
        return '', 204

