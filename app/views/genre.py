from flask import request
from flask_restx import Resource, Namespace

from app.container import genre_service
from app.dao.model.genre import genre_schema, genres_schema

genre_ns = Namespace('genres')


@genre_ns.route('/')
class GenreView(Resource):

    def get(self):
        genre = genre_service.get_all()
        return genres_schema.dump(genre), 200

    def post(self):
        req = request.get_json()
        genre = genre_service.create(req)
        return '', 201, {'location': f'/genres/{genre}'}


@genre_ns.route('/<int:pk>')
class GenreVies(Resource):

    def get(self, pk):
        genre = genre_service.get_one(pk)
        return genre_schema.dump(genre), 200

    def put(self, pk):
        req = request.get_json()
        req['id'] = pk 
       
        genre_service.update(req)
        return '', 204

    def delete(self, pk):
        genre_service.delete(pk)
        return '', 204

