from flask import request
from flask_restx import Resource, Namespace

from app.container import movie_service
from app.dao.model.movie import movie_schema, movies_schema

movie_ns = Namespace('movies')


@movie_ns.route('/')
class MoviesView(Resource):

    def get(self):
        genre = request.args.get('genre_id')
        director = request.args.get('director_id')
        year = request.args.get('year')

        if genre:
            movie = movie_service.get_genre(genre)
        elif director:
            movie = movie_service.get_director(director)
        elif year:
            movie = movie_service.get_year(year)
        else:
            movie = movie_service.get_all()
        return movies_schema.dump(movie), 200

    def post(self):
        req = request.get_json()
        movie = movie_service.create(req)
        return '', 201, {'location': f'/movies/{movie}'}


@movie_ns.route('/<int:pk>')
class MovieVies(Resource):

    def get(self, pk):
        movie = movie_service.get_one(pk)
        return movie_schema.dump(movie), 200

    def put(self, pk):
        req = request.get_json()
        req['id'] = pk 
       
        movie_service.update(req)
        return '', 204
    
    def patch(self, pk):
        req = request.get_json() 
        req['id'] = pk
        movie_service.update_partial(req)
        return '', 204

    def delete(self, pk):
        movie_service.delete(pk)
        return '', 204

