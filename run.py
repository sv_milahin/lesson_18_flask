from app import create_app


app = create_app()


@app.run('/test')
def index():
    return "test"


if __name__ == '__main__':
    app.run()

